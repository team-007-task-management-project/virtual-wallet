# Team7: Borislav & Preslav - Virtual Wallet

**Virtual Wallet**

*Web application which allows users to add money to their profile and exchange money with their friends or other users. In order to add money to their profile, the user must first provide a valid debit/credit card. They can work with more than one card. Also, in order for the users to be able to make transactions, they must first confirm their registration by verifying their email. Users also receive notifications in the app and receive an email when they receive money from other users. Each user also has a history of transactions they've made or received. Users also have a contact list where they can view a list of users they've previously sent money to. Each user can upload a profile picture and edit their profile.*


***Developers:*** *Borislav & Preslav*


***Technologies used:***
*  Java
*  Spring Boot
*  Spring MVC
*  Thymeleaf
*  HTML/CSS
*  MariaDB
*  Mockito


***To run the project:***
1.  You need Java 11, JDK, IntelliJ
2.  Clone the project on your PC/Laptop
3.  Create the database with the script create_db.sql inside the "db" folder
4.  Insert the data with the script insert_data.sql inside "db" folder


***Swagger: http://localhost:8080/swagger-ui/index.html***


***Database Relations Image:***  
![](VirtualWallet/db/DB-Relations.png)


