package com.virtualwallet.services;

import com.virtualwallet.models.Notification;
import com.virtualwallet.models.Transaction;
import com.virtualwallet.models.User;
import com.virtualwallet.repositories.interfaces.NotificationRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.virtualwallet.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class NotificationServiceImplTests {

    @Mock
    NotificationRepository mockNotificationRepository;

    @Mock
    EmailSenderServiceImpl mockEmailSenderService;

    @InjectMocks
    NotificationServiceImpl mockNotificationService;

    @Test
    void create_should_callRepository() {
        Notification mockNotification = createMockNotification();
        Transaction mockTransaction = createMockTransaction();

        mockNotificationService.create(mockNotification, mockTransaction);

        Mockito.verify(mockNotificationRepository, Mockito.times(1)).create(mockNotification);
    }

    @Test
    void update_should_callRepository() {
        Notification mockNotification = createMockNotification();

        mockNotificationService.update(mockNotification);

        Mockito.verify(mockNotificationRepository, Mockito.times(1)).update(mockNotification);
    }

    @Test
    void getAllForUser_should_callRepository() {
        User mockUser = createMockUser();

        Mockito.when(mockNotificationRepository.getAllForUser(mockUser.getId())).thenReturn(new ArrayList<>());

        mockNotificationService.getAllForUser(mockUser.getId());

        Mockito.verify(mockNotificationRepository, Mockito.times(1)).getAllForUser(mockUser.getId());
    }

    @Test
    void getUnread_should_callRepository() {
        User mockUser = createMockUser();

        Mockito.when(mockNotificationRepository.getUnread(mockUser.getId())).thenReturn(Mockito.anyLong());

        mockNotificationService.getUnread(mockUser.getId());

        Mockito.verify(mockNotificationRepository, Mockito.times(1)).getUnread(mockUser.getId());
    }

    @Test
    void deleteAllUserNotifications_should_callRepository() {
        User mockUser = createMockUser();

        mockNotificationService.deleteAllUserNotifications(mockUser.getId());

        Mockito.verify(mockNotificationRepository, Mockito.times(1)).deleteAllUserNotifications(mockUser.getId());
    }

    @Test
    void markAsRead_should_callRepository() {
        User mockUser = createMockUser();
        Notification mockNotification = createMockNotification();

        mockNotificationService.markAsRead(mockNotification, mockUser.getId());

        Mockito.verify(mockNotificationRepository, Mockito.times(1)).update(mockNotification);
    }

    @Test
    void delete_should_callRepository() {
        User mockUser = createMockUser();
        Notification mockNotification = createMockNotification();

        mockNotificationService.delete(mockNotification, mockUser.getId());

        Mockito.verify(mockNotificationRepository, Mockito.times(1)).delete(mockNotification);
    }
}
