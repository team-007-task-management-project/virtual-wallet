package com.virtualwallet.services;

import com.virtualwallet.exceptions.InsufficientFundsException;
import com.virtualwallet.exceptions.WalletNotEmptyException;
import com.virtualwallet.models.Transaction;
import com.virtualwallet.models.User;
import com.virtualwallet.models.Wallet;
import com.virtualwallet.repositories.interfaces.WalletRepository;
import com.virtualwallet.services.interfaces.CardService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;

import static com.virtualwallet.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class WalletServiceImplTests {

    @Mock
    WalletRepository mockWalletRepository;

    @Mock
    CardService mockCardService;

    @InjectMocks
    WalletServiceImpl mockWalletService;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockWalletRepository.getAll())
                .thenReturn(new ArrayList<>());

        mockWalletService.getAll();

        Mockito.verify(mockWalletRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_returnWallet_when_matchExist() {
        Wallet mockWallet = createMockWallet();

        Mockito.when(mockWalletRepository.getById(mockWallet.getId()))
                .thenReturn(mockWallet);

        Wallet result = mockWalletService.getById(mockWallet.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockWallet.getId(), result.getId()),
                () -> Assertions.assertEquals(mockWallet.getUser(), result.getUser()),
                () -> Assertions.assertEquals(mockWallet.getBalance(), result.getBalance())
        );
    }

    @Test
    void getByUserId_should_callRepository() {
        User mockUser = createMockUser();

        Mockito.when(mockWalletRepository.getByField(Mockito.anyString(),Mockito.anyInt()))
                .thenReturn(new Wallet());

        mockWalletService.getByUserId(mockUser.getId());

        Mockito.verify(mockWalletRepository, Mockito.times(1))
                .getByField(Mockito.anyString(),Mockito.anyInt());
    }

    @Test
    void doubleUpdate_should_callRepository() {
        Wallet mockWallet1 = createMockWallet();
        Wallet mockWallet2 = createMockWallet();
        mockWallet2.setId(2);
        Transaction mockTransaction = createMockTransaction();

        Mockito.when(mockWalletRepository.getByField(Mockito.anyString(), Mockito.anyInt())).thenReturn(mockWallet1, mockWallet2);
        mockWalletService.doubleUpdate(mockTransaction);

        Mockito.verify(mockWalletRepository, Mockito.times(1)).update(mockWallet1, mockWallet2);
    }

    @Test
    void doubleUpdate_throw_when_SenderWalletHasLessFunds() {
        Wallet mockWallet1 = createMockWallet();
        mockWallet1.setBalance(BigDecimal.valueOf(500));
        Wallet mockWallet2 = createMockWallet();
        mockWallet2.setId(2);
        Transaction mockTransaction = createMockTransaction();

        Mockito.when(mockWalletRepository.getByField(Mockito.anyString(), Mockito.anyInt())).thenReturn(mockWallet1, mockWallet2);

        Assertions.assertThrows(InsufficientFundsException.class, () -> mockWalletService.doubleUpdate(mockTransaction));
    }

    @Test
    void singleUpdate_should_callRepository() {
        Wallet mockWallet = createMockWallet();
        Transaction mockTransaction = createMockTransaction();

        Mockito.when(mockWalletRepository.getByField(Mockito.anyString(), Mockito.anyInt())).thenReturn(mockWallet);
        mockWalletService.singleUpdate(mockTransaction);

        Mockito.verify(mockWalletRepository, Mockito.times(1)).update(mockWallet);
    }

    @Test
    void create_should_callRepository() {
        Wallet mockWallet = createMockWallet();

        mockWalletService.create(mockWallet);

        Mockito.verify(mockWalletRepository, Mockito.times(1)).create(mockWallet);
    }

    @Test
    void delete_should_callRepository() {
        Wallet mockWallet = createMockWallet();
        mockWallet.setBalance(BigDecimal.ZERO);

        mockWalletService.delete(mockWallet);

        Mockito.verify(mockWalletRepository, Mockito.times(1)).delete(mockWallet);
    }

    @Test
    void delete_throw_when_walletHasFunds() {
        Wallet mockWallet = createMockWallet();

        Assertions.assertThrows(WalletNotEmptyException.class, () -> mockWalletService.delete(mockWallet));
    }
}
