package com.virtualwallet.models;

public enum Status {
    BLOCKED("Blocked"), UNBLOCKED("Unblocked");

    private final String displayValue;

    Status(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
