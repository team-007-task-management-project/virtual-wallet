package com.virtualwallet.models;

public enum CardStatus {
    ACTIVE("Active"), EXPIRED("Expired");

    private final String displayValue;

    CardStatus(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
