package com.virtualwallet.models;

public enum Type {
    CREDIT("Credit"), DEBIT("Debit");

    private final String displayValue;

    Type(String displayValue) {
        this.displayValue = displayValue;
    }

    public String getDisplayValue() {
        return displayValue;
    }
}
