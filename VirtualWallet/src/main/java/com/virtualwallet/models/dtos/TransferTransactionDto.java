package com.virtualwallet.models.dtos;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class TransferTransactionDto {

    private static final String INVALID_AMOUNT_ERROR_MESSAGE = "Amount cannot be 0 or negative";

    @NotEmpty
    private String sender;

    @NotEmpty
    private String recipient;

    @NotNull
    @DecimalMin(value = "0.0", inclusive = false, message = INVALID_AMOUNT_ERROR_MESSAGE)
    private BigDecimal amount;

    public TransferTransactionDto() {
    }

    public TransferTransactionDto(String recipient) {
        setRecipient(recipient);
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
