package com.virtualwallet.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class DepositTransactionDto extends TransferTransactionDto{

    private static final String NO_CARD_SELECTED_ERROR_MESSAGE = "Please select a card.";

    @NotNull
    @Positive(message = NO_CARD_SELECTED_ERROR_MESSAGE)
    private int cardId;

    public DepositTransactionDto() {
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }
}
