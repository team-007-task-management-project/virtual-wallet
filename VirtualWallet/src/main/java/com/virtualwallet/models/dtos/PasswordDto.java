package com.virtualwallet.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import static com.virtualwallet.models.dtos.UserDto.*;

public class PasswordDto {

    private String userPassword;

    @NotNull(message = PASSWORD_INVALID)
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,}$", message = PASSWORD_PATTERN_INVALID)
    private String newPassword;

    @NotNull
    private String confirmPassword;

    public PasswordDto() {
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }
}
