package com.virtualwallet.models.dtos;

import com.virtualwallet.models.Direction;
import com.virtualwallet.models.Transaction;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

import static com.virtualwallet.controllers.rest.TransactionRestController.DATE_TIME_FORMAT_PATTERN;

public class ViewTransactionDto {

    private int id;

    private String sender;

    private String recipient;

    @DateTimeFormat(pattern = DATE_TIME_FORMAT_PATTERN)
    private LocalDate date;

    private BigDecimal amount;

    private Direction direction;

    public ViewTransactionDto(Transaction transaction, int id) {
        setId(transaction.getId());
        setSender(transaction.getSender().getUsername());
        setRecipient(transaction.getRecipient().getUsername());
        setDate(transaction.getDate());
        setAmount(transaction.getAmount());
        if (transaction.getRecipient().getId() == id) {
            setDirection(Direction.INCOMING);
        } else {
            setDirection(Direction.OUTGOING);
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
