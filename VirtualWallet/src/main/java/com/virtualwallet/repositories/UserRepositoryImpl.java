package com.virtualwallet.repositories;

import com.virtualwallet.exceptions.EntityNotFoundException;
import com.virtualwallet.models.State;
import com.virtualwallet.models.User;
import com.virtualwallet.repositories.interfaces.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl extends BaseModifyRepositoryImpl<User> implements UserRepository {

    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
    }

    @Override
    public List<User> search(Optional<String> keyWord) {
        try (Session session = getSessionFactory().openSession()) {
            Query<User> query = session.createQuery("from User where state = 'ACTIVATED' and ((:keyWord = '%%') OR (username like :keyWord) OR (email like :keyWord) OR (phoneNumber like :keyWord))", User.class);
            query.setParameter("keyWord", "%" + keyWord.orElse("") + "%");

            return query.list();
        }
    }

    public User getByAllFields(String value) {
        try (Session session = getSessionFactory().openSession()) {
            Query<User> query = session.createQuery("from User where state = 'ACTIVATED' and (username = :value) OR (phoneNumber = :value) OR (email = :value)", User.class);
            query.setParameter("value", value);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("User", "username, email or phone number:", value);
            }

            return query.list().get(0);
        }
    }
}
