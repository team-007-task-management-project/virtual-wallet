package com.virtualwallet.repositories.interfaces;

import com.virtualwallet.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends BaseGetRepository<User>, CreatableRepository<User>, UpdatableRepository<User>, DeletableRepository<User> {
    User getByAllFields(String value);

    List<User> search(Optional<String> keyWord);
}
