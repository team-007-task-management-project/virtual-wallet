package com.virtualwallet.repositories.interfaces;

import java.util.List;

public interface BaseGetRepository<T> {
    List<T> getAll();

    <V> T getByField(String fieldName, V fieldValue);

    T getById(int id);
}
