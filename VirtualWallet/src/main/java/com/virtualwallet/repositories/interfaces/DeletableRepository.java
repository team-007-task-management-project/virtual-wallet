package com.virtualwallet.repositories.interfaces;

public interface DeletableRepository<T> {
    void delete(T entity);
}
