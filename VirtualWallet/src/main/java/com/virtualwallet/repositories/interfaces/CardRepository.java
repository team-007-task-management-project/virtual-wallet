package com.virtualwallet.repositories.interfaces;

import com.virtualwallet.models.Card;
import com.virtualwallet.models.User;

import java.util.List;

public interface CardRepository extends BaseGetRepository<Card>, CreatableRepository<Card>, DeletableRepository<Card> {
    List<Card> getAllForUser(int userId);

    void deleteAllForUser(int userId);
}
