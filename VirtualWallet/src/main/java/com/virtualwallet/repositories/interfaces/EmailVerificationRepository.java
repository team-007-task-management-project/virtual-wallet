package com.virtualwallet.repositories.interfaces;

import com.virtualwallet.models.EmailVerification;

public interface EmailVerificationRepository extends BaseGetRepository<EmailVerification>, CreatableRepository<EmailVerification> {
}
