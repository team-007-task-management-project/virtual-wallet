package com.virtualwallet.repositories;

import com.virtualwallet.models.Card;
import com.virtualwallet.repositories.interfaces.CardRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CardRepositoryImpl extends BaseModifyRepositoryImpl<Card> implements CardRepository {

    public CardRepositoryImpl(SessionFactory sessionFactory) {
        super(Card.class, sessionFactory);
    }

    @Override
    public List<Card> getAllForUser(int userId) {
        try (Session session = getSessionFactory().openSession()) {
            Query<Card> query = session.createQuery("from Card where user.id = :userId", Card.class);
            query.setParameter("userId", userId);

            return query.list();
        }
    }

    @Override
    public void deleteAllForUser(int userId) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            Query query = session.createQuery("delete from Card where user.id = :userId");
            query.setParameter("userId", userId);
            query.executeUpdate();
            session.getTransaction().commit();
        }
    }
}
