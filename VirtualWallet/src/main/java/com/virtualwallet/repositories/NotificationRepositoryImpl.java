package com.virtualwallet.repositories;

import com.virtualwallet.models.Notification;
import com.virtualwallet.repositories.interfaces.NotificationRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class NotificationRepositoryImpl extends BaseModifyRepositoryImpl<Notification> implements NotificationRepository {

    private final SessionFactory sessionFactory;

    public NotificationRepositoryImpl(SessionFactory sessionFactory) {
        super(Notification.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Notification> getAllForUser(int userId) {
        try (Session session = getSessionFactory().openSession()) {
            Query<Notification> query = session.createQuery("from Notification where user.id = :userId order by creationDate desc", Notification.class);
            query.setParameter("userId", userId);

            return query.list();
        }
    }

    @Override
    public Long getUnread(int userId) {
        try (Session session = getSessionFactory().openSession()) {
            Query query = session.createQuery("select count(*) from Notification where user.id = :userId and isRead = false");
            query.setParameter("userId", userId);

            return (Long) query.uniqueResult();
        }
    }

    @Override
    public void deleteAllUserNotifications(int userId) {
        try (Session session = getSessionFactory().openSession()) {
            session.beginTransaction();
            Query query = session.createQuery("delete from Notification where user.id = :userId");
            query.setParameter("userId", userId);
            query.executeUpdate();
            session.getTransaction().commit();
        }
    }
}
