package com.virtualwallet.repositories;

import com.virtualwallet.models.EmailVerification;
import com.virtualwallet.repositories.interfaces.EmailVerificationRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class EmailVerificationRepositoryImpl extends BaseModifyRepositoryImpl<EmailVerification> implements EmailVerificationRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public EmailVerificationRepositoryImpl(SessionFactory sessionFactory) {
        super(EmailVerification.class, sessionFactory);
        this.sessionFactory = sessionFactory;

    }
}
