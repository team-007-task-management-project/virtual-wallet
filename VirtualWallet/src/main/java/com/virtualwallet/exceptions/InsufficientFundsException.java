package com.virtualwallet.exceptions;

public class InsufficientFundsException extends RuntimeException{

    private static final String INSUFFICIENT_AMOUNT_ERROR_MESSAGE = "Insufficient funds";

    public InsufficientFundsException() {
        super(INSUFFICIENT_AMOUNT_ERROR_MESSAGE);
    }
}
