package com.virtualwallet.mappers;

import com.virtualwallet.models.Card;
import com.virtualwallet.models.Type;
import com.virtualwallet.models.User;
import com.virtualwallet.models.dtos.CardDto;
import com.virtualwallet.models.dtos.ViewCardDto;
import com.virtualwallet.services.interfaces.CardService;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Component
public class CardMapper {

    public CardMapper() {
    }

    public Card fromCardDtoToCardObject(CardDto cardDto, User user) {
        Card card = new Card();
        card.setUser(user);
        card.setCardHolder(cardDto.getCardHolder());
        card.setCardNumber(cardDto.getCardNumber());
        YearMonth ym = YearMonth.parse(cardDto.getExpirationDate(), DateTimeFormatter.ofPattern("MM/yy"));
        LocalDate expirationDate = ym.atEndOfMonth();
        card.setExpirationDate(expirationDate);
        card.setCheckNumber(cardDto.getCheckNumber());
        card.setType(Type.valueOf(cardDto.getType().toString().toUpperCase()));
        return card;
    }

    public List<ViewCardDto> convertList(List<Card> list) {
        List<ViewCardDto> result = new ArrayList<>();

        for (Card card : list) {
            result.add(new ViewCardDto(card));
        }


        return result;
    }
}
