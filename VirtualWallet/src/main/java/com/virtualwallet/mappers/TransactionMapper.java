package com.virtualwallet.mappers;

import com.virtualwallet.models.Transaction;
import com.virtualwallet.models.User;
import com.virtualwallet.models.dtos.TransferTransactionDto;
import com.virtualwallet.models.dtos.ViewTransactionDto;
import com.virtualwallet.services.interfaces.UserService;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class TransactionMapper {

    private final UserService userService;

    public TransactionMapper(UserService userService) {
        this.userService = userService;
    }

    public Transaction fromTransactionDtoToTransactionObject(TransferTransactionDto transactionDto) {
        Transaction transaction = new Transaction();
        User sender = userService.getByUsername(transactionDto.getSender());
        User recipient = userService.getByAllFields(transactionDto.getRecipient());
        transaction.setSender(sender);
        transaction.setRecipient(recipient);
        transaction.setDate(LocalDate.now());
        transaction.setAmount(transactionDto.getAmount());

        return transaction;
    }

    public List<ViewTransactionDto> convertList(List<Transaction> list, int id) {
        List<ViewTransactionDto> result = new ArrayList<>();

        for (Transaction transaction : list) {
            result.add(new ViewTransactionDto(transaction, id));
        }
        Collections.reverse(result);

        return result;
    }
}
