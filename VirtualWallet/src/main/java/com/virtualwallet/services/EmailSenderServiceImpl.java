package com.virtualwallet.services;

import com.virtualwallet.models.EmailVerification;
import com.virtualwallet.models.Notification;
import com.virtualwallet.models.Transaction;
import com.virtualwallet.models.User;
import com.virtualwallet.services.interfaces.EmailSenderService;
import com.virtualwallet.services.interfaces.EmailVerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;

@Service
@PropertySource("classpath:messages.properties")
public class EmailSenderServiceImpl implements EmailSenderService {

    @Value("vwalletconfirmation@gmail.com")
    private String sender;
    private final JavaMailSender javaMailSender;
    private final EmailVerificationService emailVerificationService;

    @Autowired
    public EmailSenderServiceImpl(JavaMailSender javaMailSender, EmailVerificationService emailVerificationService) {
        this.javaMailSender = javaMailSender;
        this.emailVerificationService = emailVerificationService;
    }

    @Override
    public void sendEmailConfirmation(User user) {
        EmailVerification emailVerification = emailVerificationService.create(user);
        String token = emailVerification.getTokenValue();
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(user.getEmail()));
            mimeMessage.setFrom(new InternetAddress(sender));
            mimeMessage.setSubject("Confirm your Registration!");
            mimeMessage.setContent("To confirm your registration please click on the following link: \n" +
                    "http://localhost:8080/auth/email-confirmation?token=" + token, "text/html; charset=utf-8");
        };
        sendMail(messagePreparator);
    }


    @Override
    public void sendMail(MimeMessagePreparator email) {
        javaMailSender.send(email);
    }

    @Override
    public void sendEmailNotification(Notification notification, Transaction transaction) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(notification.getUser().getEmail()));
            mimeMessage.setFrom(new InternetAddress(sender));
            mimeMessage.setSubject(String.format("New transaction from %s.", transaction.getSender().getUsername()));
            mimeMessage.setContent(notification.getMessage(), "text/html; charset=utf-8");
        };
        sendMail(messagePreparator);
    }
}