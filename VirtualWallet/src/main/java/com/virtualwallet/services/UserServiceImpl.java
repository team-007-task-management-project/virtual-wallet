package com.virtualwallet.services;

import com.virtualwallet.controllers.CloudinaryHelper;
import com.virtualwallet.exceptions.DuplicateEntityException;
import com.virtualwallet.exceptions.EntityNotFoundException;
import com.virtualwallet.models.EmailVerification;
import com.virtualwallet.models.State;
import com.virtualwallet.models.User;
import com.virtualwallet.models.Wallet;
import com.virtualwallet.repositories.interfaces.EmailVerificationRepository;
import com.virtualwallet.repositories.interfaces.UserRepository;
import com.virtualwallet.services.interfaces.CardService;
import com.virtualwallet.services.interfaces.EmailSenderService;
import com.virtualwallet.services.interfaces.UserService;
import com.virtualwallet.services.interfaces.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.web.multipart.MultipartFile;

@Service
public class UserServiceImpl extends BaseGetServiceImpl<User> implements UserService {

    private final UserRepository repository;
    private final WalletService walletService;
    private final CardService cardService;
    private final CloudinaryHelper cloudinaryHelper;
    private final EmailSenderService emailSenderService;
    private final EmailVerificationRepository emailVerificationRepository;

    @Autowired
    public UserServiceImpl(UserRepository repository, WalletService walletService, CardService cardService, CloudinaryHelper cloudinaryHelper, EmailSenderService emailSenderService, EmailVerificationRepository emailVerificationRepository) {
        super(repository);
        this.repository = repository;
        this.walletService = walletService;
        this.cardService = cardService;
        this.cloudinaryHelper = cloudinaryHelper;
        this.emailSenderService = emailSenderService;
        this.emailVerificationRepository = emailVerificationRepository;
    }

    @Override
    public User getByUsername(String username) {
        return repository.getByField("username", username);
    }

    @Override
    public User getByAllFields(String value) {
        return repository.getByAllFields(value);
    }

    @Override
    public List<User> search(Optional<String> keyWord) {
        return repository.search(keyWord);
    }

    @Override
    public void upload(MultipartFile file, User user) throws IOException {
        if (file.isEmpty()) {
            throw new FileNotFoundException();
        }
        String url = cloudinaryHelper.upload(file);
        user.setAvatarUrl(url);
        repository.update(user);
    }

    @Override
    public void create(User user) {
        checkIfUserIsValid(user);
        repository.create(user);
        Wallet wallet = new Wallet(BigDecimal.ZERO, user);
        walletService.create(wallet);
        emailSenderService.sendEmailConfirmation(user);
    }

    @Override
    public void confirmUser(String uuid) {
        EmailVerification emailVerification = emailVerificationRepository.getByField("tokenValue", uuid);
        User user = emailVerification.getUser();
        if (user.isEmailConfirmed()) {
            throw new IllegalArgumentException("You have already confirmed your Email!");
        } else {
            user.setEmailConfirmed(true);
            repository.update(user);
        }
    }

    @Override
    public void update(User user) {
        checkDuplicateEmail(user);
        checkDuplicatePhoneNumber(user);
        repository.update(user);
    }

    @Override
    public void delete(User user) {
        Wallet wallet = walletService.getByUserId(user.getId());
        cardService.deleteAllForUser(user.getId());
        walletService.delete(wallet);
        user.setState(State.DEACTIVATED);
        repository.update(user);
    }

    private void checkIfUserIsValid(User user) {
        checkDuplicateUsername(user);
        checkDuplicateEmail(user);
        checkDuplicatePhoneNumber(user);
    }

    private void checkDuplicateUsername(User user) {
        boolean duplicateExists = true;
        try {
            User existingUser = repository.getByField("username", user.getUsername());
            if (existingUser.getId() == user.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }
    }

    private void checkDuplicateEmail(User user) {
        boolean duplicateExists = true;
        try {
            User existingUser = repository.getByField("email", user.getEmail());
            if (existingUser.getId() == user.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }
    }

    private void checkDuplicatePhoneNumber(User user) {
        boolean duplicateExists = true;
        try {
            User existingUser = repository.getByField("phoneNumber", user.getPhoneNumber());
            if (existingUser.getId() == user.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "phone number", user.getPhoneNumber());
        }
    }
}
