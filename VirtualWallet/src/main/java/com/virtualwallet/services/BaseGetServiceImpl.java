package com.virtualwallet.services;

import com.virtualwallet.repositories.interfaces.BaseGetRepository;
import com.virtualwallet.services.interfaces.BaseGetService;

import java.util.List;

public abstract class BaseGetServiceImpl<T> implements BaseGetService<T> {

    private final BaseGetRepository<T> repository;

    public BaseGetServiceImpl(BaseGetRepository<T> repository) {
        this.repository = repository;
    }

    @Override
    public List<T> getAll() {
        return repository.getAll();
    }

    @Override
    public T getById(int id) {
        return repository.getById(id);
    }
}
