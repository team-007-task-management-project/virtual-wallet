package com.virtualwallet.services;

import com.virtualwallet.models.EmailVerification;
import com.virtualwallet.models.User;
import com.virtualwallet.repositories.interfaces.BaseGetRepository;
import com.virtualwallet.repositories.interfaces.EmailVerificationRepository;
import com.virtualwallet.services.interfaces.EmailVerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmailVerificationServiceImpl extends BaseGetServiceImpl<EmailVerification> implements EmailVerificationService {

    private final EmailVerificationRepository emailVerificationRepository;

    @Autowired
    public EmailVerificationServiceImpl(BaseGetRepository<EmailVerification> repository, EmailVerificationRepository emailVerificationRepository) {
        super(repository);
        this.emailVerificationRepository = emailVerificationRepository;
    }

    @Override
    public EmailVerification create(User user) {
        EmailVerification emailVerification = new EmailVerification(user);
        emailVerificationRepository.create(emailVerification);
        return emailVerification;
    }
}
