package com.virtualwallet.services.interfaces;

public interface UpdatableService<T> {
    void update(T entity);
}
