package com.virtualwallet.services.interfaces;

import com.virtualwallet.models.Card;
import com.virtualwallet.models.User;

import java.util.List;

public interface CardService extends BaseGetService<Card>, CreatableService<Card> {
    List<Card> getAllForUser(int userId);

    void deleteAllForUser(int userId);

    void delete(Card entity, int userID);

    Card getValidCard(int cardId);
}
