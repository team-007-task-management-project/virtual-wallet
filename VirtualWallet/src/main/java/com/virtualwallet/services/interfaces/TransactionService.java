package com.virtualwallet.services.interfaces;

import com.virtualwallet.models.Transaction;
import com.virtualwallet.models.User;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface TransactionService extends BaseGetService<Transaction>, CreatableService<Transaction> {

    List<Transaction> getAllForUser(User authenticatedUser,
                                    int id,
                                    Optional<LocalDate> fromDate,
                                    Optional<LocalDate> toDate,
                                    Optional<Integer> senderId,
                                    Optional<Integer> recipientId,
                                    Optional<String> sortAmount,
                                    Optional<String> sortDate);

    List<Transaction> getLastThree(int userId);
}
