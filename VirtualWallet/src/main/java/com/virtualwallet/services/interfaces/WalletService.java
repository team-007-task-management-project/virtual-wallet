package com.virtualwallet.services.interfaces;

import com.virtualwallet.models.Transaction;
import com.virtualwallet.models.Wallet;

public interface WalletService extends CreatableService<Wallet>, DeletableService<Wallet> {
    Wallet getByUserId(int id);

    void doubleUpdate(Transaction transaction);

    void singleUpdate(Transaction transaction);
}
