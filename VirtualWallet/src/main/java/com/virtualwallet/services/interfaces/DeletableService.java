package com.virtualwallet.services.interfaces;

public interface DeletableService<T> {
    void delete(T entity);
}
