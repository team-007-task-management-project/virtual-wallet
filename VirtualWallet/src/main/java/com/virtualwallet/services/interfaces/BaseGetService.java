package com.virtualwallet.services.interfaces;

import java.util.List;

public interface BaseGetService<T> {

    List<T> getAll();

    T getById(int id);
}
