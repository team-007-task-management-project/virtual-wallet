package com.virtualwallet.services.interfaces;

import com.virtualwallet.models.Notification;
import com.virtualwallet.models.Transaction;
import com.virtualwallet.models.User;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;

import java.util.Optional;

public interface EmailSenderService {
    void sendEmailConfirmation(User user);

    void sendMail(MimeMessagePreparator email);

    void sendEmailNotification(Notification notification, Transaction transaction);
}
