package com.virtualwallet.services.interfaces;

import com.virtualwallet.models.Notification;
import com.virtualwallet.models.Transaction;

import java.util.List;

public interface NotificationService extends BaseGetService<Notification>, UpdatableService<Notification> {

    void create(Notification notification, Transaction transaction);

    List<Notification> getAllForUser(int userId);

    Long getUnread(int userId);

    void deleteAllUserNotifications(int userId);

    void markAsRead(Notification notification, int userId);

    void delete(Notification notification, int userId);
}
