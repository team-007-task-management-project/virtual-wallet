package com.virtualwallet.services.interfaces;

public interface CreatableService<T> {
    void create(T entity);
}
