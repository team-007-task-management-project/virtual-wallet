package com.virtualwallet.services;

import com.virtualwallet.exceptions.InsufficientFundsException;
import com.virtualwallet.exceptions.WalletNotEmptyException;
import com.virtualwallet.models.Transaction;
import com.virtualwallet.models.Wallet;
import com.virtualwallet.repositories.interfaces.WalletRepository;
import com.virtualwallet.services.interfaces.CardService;
import com.virtualwallet.services.interfaces.WalletService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class WalletServiceImpl extends BaseGetServiceImpl<Wallet> implements WalletService {

    private final WalletRepository repository;
    private final CardService cardService;

    @Autowired
    public WalletServiceImpl(WalletRepository repository, CardService cardService) {
        super(repository);
        this.repository = repository;
        this.cardService = cardService;
    }

    @Override
    public Wallet getByUserId(int id) {
        return repository.getByField("user_id", id);
    }

    @Override
    public void doubleUpdate(Transaction transaction) {
        Wallet senderWallet = getByUserId(transaction.getSender().getId());

        if (hasLessFunds(transaction, senderWallet)) {
            throw new InsufficientFundsException();
        }

        senderWallet.setBalance(senderWallet.getBalance().subtract(transaction.getAmount()));
        Wallet recipientWallet = getByUserId(transaction.getRecipient().getId());

        recipientWallet.setBalance(recipientWallet.getBalance().add(transaction.getAmount()));
        repository.update(senderWallet, recipientWallet);
    }

    @Override
    public void singleUpdate(Transaction transaction) {
        Wallet recipientWallet = getByUserId(transaction.getRecipient().getId());
        recipientWallet.setBalance(recipientWallet.getBalance().add(transaction.getAmount()));

        repository.update(recipientWallet);
    }

    @Override
    public void create(Wallet wallet) {
        repository.create(wallet);
    }

    @Override
    public void delete(Wallet wallet) {
        if (wallet.getBalance().compareTo(BigDecimal.ZERO) > 0) {
            throw new WalletNotEmptyException();
        }
        repository.delete(wallet);
    }

    private boolean hasLessFunds(Transaction transaction, Wallet senderWallet) {
        return senderWallet.getBalance().compareTo(transaction.getAmount()) < 0;
    }
}
