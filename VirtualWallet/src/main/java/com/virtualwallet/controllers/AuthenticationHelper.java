package com.virtualwallet.controllers;

import com.virtualwallet.exceptions.AuthenticationFailureException;
import com.virtualwallet.exceptions.EntityNotFoundException;
import com.virtualwallet.exceptions.UnauthorizedOperationException;
import com.virtualwallet.models.User;
import com.virtualwallet.services.interfaces.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

@Component
public class AuthenticationHelper {
    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    private static final String UNAUTHORIZED_ERROR_MESSAGE = "The requested resource requires authentication.";
    private static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong username or password.";
    public static final String ROLE_INVALID = "The requested resource requires admin rights.";


    private final UserService userService;

    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, UNAUTHORIZED_ERROR_MESSAGE);
        }

        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            User user = userService.getByUsername(username);
            checkUserState(user);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    public User tryGetUser(HttpSession session) {
        String currentUser = (String) session.getAttribute("currentUser");

        if (currentUser == null) {
            throw new AuthenticationFailureException("No user logged in.");
        }

        return userService.getByUsername(currentUser);
    }

    public User tryGetAdmin(HttpSession session) {
        User user = tryGetUser(session);

        checkIfAdmin(user);
        return user;
    }

    public User tryGetAdmin(HttpHeaders headers) {
        User user = tryGetUser(headers);
        try {
            checkIfAdmin(user);
            return user;
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    public void checkIfAdmin(User user) {
        if (!user.getRole().getDisplayValue().equals("Admin")) {
            throw new UnauthorizedOperationException(ROLE_INVALID);
        }
    }

    public void checkUserState(User user) {
        if (user.getState().getDisplayValue().equals("Deactivated")) {
            throw new EntityNotFoundException("User", "username", user.getUsername());
        }
    }

    public User verifyAuthentication(String username, String password) {
        try {
            User user = userService.getByUsername(username);
            checkUserState(user);

            if (!user.getPassword().equals(password)) {
                throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
        }
    }
}
