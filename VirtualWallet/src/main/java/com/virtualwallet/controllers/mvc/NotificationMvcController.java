package com.virtualwallet.controllers.mvc;

import com.virtualwallet.controllers.AuthenticationHelper;
import com.virtualwallet.exceptions.AuthenticationFailureException;
import com.virtualwallet.exceptions.UnauthorizedOperationException;
import com.virtualwallet.models.Notification;
import com.virtualwallet.models.User;
import com.virtualwallet.services.interfaces.NotificationService;
import org.springframework.ui.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/notifications")
public class NotificationMvcController {

    private final NotificationService notificationService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public NotificationMvcController(NotificationService notificationService, AuthenticationHelper authenticationHelper) {
        this.notificationService = notificationService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("userId")
    public Integer getLoggedUserId(HttpSession session) {
        return (Integer) session.getAttribute("userId");
    }

    @ModelAttribute("isAdmin")
    public boolean isAdmin(HttpSession session) {
        return (session.getAttribute("currentUser") != null && authenticationHelper.tryGetUser(session).getRole().getDisplayValue().equals("Admin"));
    }

    @ModelAttribute("notificationsCount")
    public Long populateNotificationsNumber(HttpSession session) {
        return notificationService.getUnread((Integer) session.getAttribute("userId"));
    }

    @GetMapping
    public String showAllUserNotifications(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);

            model.addAttribute("notifications", notificationService.getAllForUser(user.getId()));

            return "notifications";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "blocked";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}/read")
    public String markAsRead(@PathVariable int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Notification notification = notificationService.getById(id);
            notificationService.markAsRead(notification, user.getId());

            return "redirect:/notifications";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "blocked";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/delete")
    public String deleteAllUserNotifications(HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            notificationService.deleteAllUserNotifications(user.getId());

            return "redirect:/notifications";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteNotification(@PathVariable int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Notification notification = notificationService.getById(id);
            notificationService.delete(notification, user.getId());

            return "redirect:/notifications";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "blocked";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }
}
