package com.virtualwallet.controllers.mvc;

import com.virtualwallet.controllers.AuthenticationHelper;
import com.virtualwallet.exceptions.AuthenticationFailureException;
import com.virtualwallet.exceptions.CardExpiredException;
import com.virtualwallet.exceptions.EntityNotFoundException;
import com.virtualwallet.exceptions.UnauthorizedOperationException;
import com.virtualwallet.mappers.CardMapper;
import com.virtualwallet.models.Card;
import com.virtualwallet.models.User;
import com.virtualwallet.models.dtos.CardDto;
import com.virtualwallet.models.dtos.ViewCardDto;
import com.virtualwallet.services.interfaces.CardService;
import com.virtualwallet.services.interfaces.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/cards")
public class CardMvcController {

    private final CardService cardService;
    private final CardMapper cardMapper;
    private final AuthenticationHelper authenticationHelper;
    private final NotificationService notificationService;

    @Autowired
    public CardMvcController(CardService cardService, CardMapper cardMapper, AuthenticationHelper authenticationHelper, NotificationService notificationService) {
        this.cardService = cardService;
        this.cardMapper = cardMapper;
        this.authenticationHelper = authenticationHelper;
        this.notificationService = notificationService;
    }

    @ModelAttribute("userId")
    public Integer getLoggedUserId(HttpSession session) {
        return (Integer) session.getAttribute("userId");
    }

    @ModelAttribute("notifications")
    public Long populateNotificationsNumber(HttpSession session) {
        return notificationService.getUnread((Integer) session.getAttribute("userId"));
    }

    @ModelAttribute("isAdmin")
    public boolean isAdmin(HttpSession session) {
        return (session.getAttribute("currentUser") != null && authenticationHelper.tryGetUser(session).getRole().getDisplayValue().equals("Admin"));
    }

    @GetMapping
    public String showAllUserCard(Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            List<ViewCardDto> list = cardMapper.convertList(cardService.getAllForUser(user.getId()));
            model.addAttribute("cards", list);

            return "cards";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/new")
    public String showNewCard(Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetUser(session);
            model.addAttribute("card", new CardDto());

            return "card-new";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }

    @PostMapping("/new")
    public String addNewCard(@Valid @ModelAttribute("card") CardDto cardDto,
                             BindingResult errors,
                             HttpSession session,
                             Model model) {

        if (errors.hasErrors()) {
            return "card-new";
        }

        try {
            User user = authenticationHelper.tryGetUser(session);
            Card card = cardMapper.fromCardDtoToCardObject(cardDto, user);

            cardService.create(card);

            return "redirect:/cards";
        } catch (IllegalArgumentException e) {
            model.addAttribute("error", e.getMessage());
            return "card-new";
        } catch (CardExpiredException e) {
            model.addAttribute("error", e.getMessage());
            return "card-expired";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteCard(@PathVariable int id, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetUser(session);
            Card card = cardService.getById(id);
            cardService.delete(card, user.getId());

            return "redirect:/cards";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "not-found";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        }
    }
}
