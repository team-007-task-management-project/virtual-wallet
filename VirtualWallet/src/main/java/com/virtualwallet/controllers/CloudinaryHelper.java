package com.virtualwallet.controllers;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

@Component
public class  CloudinaryHelper {

    private final Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
            "cloud_name", "dm9jsxeij",
            "api_key", "432177745465847",
            "api_secret", "MFN4RxMBO0p1Ap_AF2tvt_a7yus",
            "secure", true));

    public CloudinaryHelper() {
    }

    public String upload(MultipartFile file) throws IOException {
        File convertedFile = new File(System.getProperty("java.io.tmpdir")+"/"+file.getName());
        file.transferTo(convertedFile);
        var uploader = cloudinary.uploader();
        var result = uploader.upload(convertedFile, ObjectUtils.emptyMap());

        return result.get("url").toString();
    }
}
