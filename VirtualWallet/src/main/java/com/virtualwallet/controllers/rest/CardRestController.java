package com.virtualwallet.controllers.rest;

import com.virtualwallet.controllers.AuthenticationHelper;
import com.virtualwallet.exceptions.DuplicateEntityException;
import com.virtualwallet.exceptions.EntityNotFoundException;
import com.virtualwallet.exceptions.UnauthorizedOperationException;
import com.virtualwallet.mappers.CardMapper;
import com.virtualwallet.models.Card;
import com.virtualwallet.models.User;
import com.virtualwallet.models.dtos.CardDto;
import com.virtualwallet.services.interfaces.CardService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/cards")
public class CardRestController {
    private final CardService service;
    private final CardMapper cardMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public CardRestController(CardService service, CardMapper cardMapper, AuthenticationHelper authenticationHelper) {
        this.service = service;
        this.cardMapper = cardMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @ApiOperation(value = "Returns all cards.")
    @GetMapping
    public List<Card> getAll(@RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetAdmin(headers);

            return service.getAll();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns a card based on id.")
    @GetMapping("/{id}")
    public Card getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            authenticationHelper.tryGetAdmin(headers);

            return service.getById(id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Creates a card.")
    @PostMapping
    public Card create(@RequestHeader HttpHeaders headers, @Valid @RequestBody CardDto cardDto) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);
            Card card = cardMapper.fromCardDtoToCardObject(cardDto, authenticatedUser);
            service.create(card);

            return card;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @ApiOperation(value = "Deletes a card.")
    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Card card = service.getById(id);

            service.delete(card, user.getId());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
