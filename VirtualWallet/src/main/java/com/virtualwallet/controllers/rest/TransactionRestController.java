package com.virtualwallet.controllers.rest;

import com.virtualwallet.controllers.AuthenticationHelper;
import com.virtualwallet.exceptions.*;
import com.virtualwallet.mappers.TransactionMapper;
import com.virtualwallet.models.Transaction;
import com.virtualwallet.models.User;
import com.virtualwallet.models.dtos.DepositTransactionDto;
import com.virtualwallet.models.dtos.TransferTransactionDto;
import com.virtualwallet.models.dtos.ViewTransactionDto;
import com.virtualwallet.services.interfaces.CardService;
import com.virtualwallet.services.interfaces.TransactionService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/transactions")
public class TransactionRestController {

    private static final String NEGATIVE_AMOUNT_ERROR_MESSAGE = "Amount must be positive";
    public static final String DATE_TIME_FORMAT_PATTERN = "yyyy-MM-dd";

    private final TransactionService service;
    private final AuthenticationHelper authenticationHelper;
    private final TransactionMapper transactionMapper;
    private final CardService cardService;

    @Autowired
    public TransactionRestController(TransactionService service, AuthenticationHelper authenticationHelper, TransactionMapper transactionMapper, CardService cardService) {
        this.service = service;
        this.authenticationHelper = authenticationHelper;
        this.transactionMapper = transactionMapper;
        this.cardService = cardService;
    }

    @ApiOperation(value = "Returns all transactions.")
    @GetMapping
    public List<Transaction> getAll(@RequestHeader HttpHeaders headers) {
        try {
            authenticationHelper.tryGetAdmin(headers);

            return service.getAll();
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns a transaction based on id.")
    @GetMapping("/{id}")
    public Transaction getById(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            authenticationHelper.tryGetAdmin(headers);

            return service.getById(id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Returns all transactions for a specific user filtered by date, sender, recipient and/or direction.")
    @GetMapping("/user/{id}")
    public List<ViewTransactionDto> getAllForUser(@RequestHeader HttpHeaders headers,
                                                  @PathVariable int id,
                                                  @RequestParam(required = false)
                                                  @DateTimeFormat(pattern = DATE_TIME_FORMAT_PATTERN)
                                                          Optional<LocalDate> fromDate,
                                                  @RequestParam(required = false)
                                                  @DateTimeFormat(pattern = DATE_TIME_FORMAT_PATTERN)
                                                          Optional<LocalDate> toDate,
                                                  @RequestParam(required = false) Optional<Integer> senderId,
                                                  @RequestParam(required = false) Optional<Integer> recipientId,
                                                  @RequestParam(required = false) Optional<String> sortAmount,
                                                  @RequestParam(required = false) Optional<String> sortDate) {
        try {
            User authenticatedUser = authenticationHelper.tryGetUser(headers);

            return transactionMapper.convertList(
                    service.getAllForUser(authenticatedUser, id, fromDate, toDate, senderId, recipientId, sortAmount, sortDate)
                    , id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @ApiOperation(value = "Creates a transfer transaction.")
    @PostMapping
    public Transaction create(@RequestHeader HttpHeaders headers, @Valid @RequestBody TransferTransactionDto transactionDto) {
        try {
            authenticationHelper.tryGetUser(headers);
            Transaction transaction = transactionMapper.fromTransactionDtoToTransactionObject(transactionDto);

            service.create(transaction);
            return transaction;
        } catch (InsufficientFundsException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @ApiOperation(value = "Creates a deposit transaction")
    @PostMapping("/deposit")
    public Transaction deposit(@RequestHeader HttpHeaders headers, @Valid @RequestBody DepositTransactionDto transactionDto) {
        try {
            authenticationHelper.tryGetUser(headers);
            cardService.getValidCard(transactionDto.getCardId());
            Transaction transaction = transactionMapper.fromTransactionDtoToTransactionObject(transactionDto);

            service.create(transaction);
            return transaction;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (ConstraintViolationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, NEGATIVE_AMOUNT_ERROR_MESSAGE);
        } catch (InsufficientFundsException | CardExpiredException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (DummyBankServerException e) {
            throw new ResponseStatusException(HttpStatus.BAD_GATEWAY, e.getMessage());
        }
    }
}
