-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.3-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table vwallet.cards: ~6 rows (approximately)
/*!40000 ALTER TABLE `cards` DISABLE KEYS */;
INSERT INTO `cards` (`card_id`, `card_holder`, `card_number`, `expiration_date`, `check_number`, `user_id`, `type`)
VALUES (1, 'Sebastian Vettel', '5169253645898712', '2024-09-30 00:00:00', '358', 4, 'DEBIT'),
       (2, 'Cristiano Ronaldo', '5876589668591523', '2022-08-31 00:00:00', '687', 5, 'DEBIT'),
       (3, 'Charles LeClerc', '1526653257685896', '2023-05-31 00:00:00', '569', 6, 'DEBIT'),
       (4, 'Felipe Massa', '5846658775896254', '2024-03-31 00:00:00', '365', 7, 'CREDIT'),
       (5, 'Fernando Alonso', '2456356958792365', '2025-03-31 00:00:00', '657', 8, 'CREDIT'),
       (6, 'Kimi Raikkonen', '5489684575682536', '2022-02-28 00:00:00', '548', 9, 'CREDIT');
/*!40000 ALTER TABLE `cards` ENABLE KEYS */;

-- Dumping data for table vwallet.email_verifications: ~2 rows (approximately)
/*!40000 ALTER TABLE `email_verifications` DISABLE KEYS */;
INSERT INTO `email_verifications` (`token_id`, `token_value`, `user_id`)
VALUES (1, 'c02e024c-5cdb-4dbc-b266-dc065c3a0184', 2),
       (2, 'ebded23c-1c19-49a6-82de-702c182dc0a0', 3);
/*!40000 ALTER TABLE `email_verifications` ENABLE KEYS */;

-- Dumping data for table vwallet.notifications: ~5 rows (approximately)
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` (`notification_id`, `message`, `user_id`, `is_read`, `creation_date`)
VALUES (13, 'You have received 2500 BGN from borislav.kibarov.', 5, 0, '2021-10-07'),
       (14, 'You have received 5632 BGN from borislav.kibarov.', 8, 0, '2021-10-07'),
       (15, 'You have received 5863 BGN from borislav.kibarov.', 9, 0, '2021-10-07'),
       (16, 'You have received 2589 BGN from borislav.kibarov.', 4, 0, '2021-10-07'),
       (18, 'You have received 250 BGN from borislav.kibarov.', 5, 0, '2021-10-07');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;

-- Dumping data for table vwallet.transactions: ~24 rows (approximately)
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
INSERT INTO `transactions` (`transaction_id`, `sender_id`, `recipient_id`, `date`, `amount`)
VALUES (1, 1, 4, '2021-10-04', 100000.00),
       (2, 1, 5, '2021-10-05', 500000.00),
       (3, 1, 6, '2021-10-03', 250000.00),
       (4, 1, 7, '2021-10-04', 330000.00),
       (5, 1, 8, '2021-10-06', 650000.00),
       (6, 1, 9, '2021-10-03', 950000.00),
       (7, 4, 3, '2021-09-14', 1000.00),
       (8, 4, 3, '2021-09-20', 1500.00),
       (9, 8, 3, '2021-09-21', 5000.00),
       (10, 8, 3, '2021-09-22', 3500.00),
       (11, 9, 3, '2021-09-19', 65000.00),
       (12, 9, 3, '2021-08-30', 2500.00),
       (13, 7, 3, '2021-09-23', 256000.00),
       (14, 7, 3, '2021-09-09', 3333.00),
       (15, 6, 3, '2021-07-05', 15000.00),
       (16, 6, 3, '2021-08-13', 25000.00),
       (17, 5, 3, '2021-09-19', 26300.00),
       (18, 5, 3, '2021-06-08', 32152.00),
       (19, 3, 5, '2021-10-05', 2500.00),
       (20, 3, 8, '2021-05-05', 5632.00),
       (21, 3, 9, '2021-09-17', 5863.00),
       (22, 3, 4, '2021-07-27', 2589.00),
       (23, 5, 3, '2021-10-04', 250.00),
       (24, 3, 5, '2021-06-10', 250.00);
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;

-- Dumping data for table vwallet.users: ~9 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `username`, `password`, `email`, `phone_number`, `status`, `role`, `state`, `image_url`,
                     `email_confirmed`)
VALUES (1, 'DummyBank', 'DummyBank7@', 'dummy.bank@gmail.com', '0883320760', 'UNBLOCKED', 'ADMIN', 'ACTIVATED',
        'https://res.cloudinary.com/dm9jsxeij/image/upload/v1633438332/nk1rzh2vnkqynagdziml.jpg', 1),
       (2, 'preslav.hristov', 'Password7@', 'p.hristov96@abv.bg', '0883320761', 'UNBLOCKED', 'ADMIN', 'ACTIVATED',
        'https://res.cloudinary.com/dm9jsxeij/image/upload/v1633438332/nk1rzh2vnkqynagdziml.jpg', 1),
       (3, 'borislav.kibarov', 'Password7@', 'emangi_30@abv.bg', '0883320762', 'UNBLOCKED', 'ADMIN', 'ACTIVATED',
        'https://res.cloudinary.com/dm9jsxeij/image/upload/v1633586840/Bobi_CV_rsc2g7.jpg', 1),
       (4, 'sebastian.vettel', 'Password7@', 'sebastian.vettel@gmail.com', '0883320763', 'UNBLOCKED', 'USER',
        'ACTIVATED', 'https://res.cloudinary.com/dm9jsxeij/image/upload/v1633205577/t3sdjiaifx4hbtbaht0s.jpg', 1),
       (5, 'cristiano.ronaldo', 'Password7@', 'cristiano.ronaldo@gmail.com', '0883320764', 'UNBLOCKED', 'USER',
        'ACTIVATED',
        'https://res.cloudinary.com/dm9jsxeij/image/upload/v1633586642/a4cbe6aac02908b9723a95b3018a1a6e_ijryci.jpg', 1),
       (6, 'charles.leclerc', 'Password7@', 'charles.leclerc@gmail.coom', '0883320766', 'UNBLOCKED', 'USER',
        'ACTIVATED', 'https://res.cloudinary.com/dm9jsxeij/image/upload/v1633586248/E4bMn_WWEAkzcD7_st2zgv.jpg', 1),
       (7, 'felipe.massa', 'Password7@', 'felipe.massa@gmail.com', '0883320767', 'UNBLOCKED', 'USER', 'ACTIVATED',
        'https://res.cloudinary.com/dm9jsxeij/image/upload/v1633586179/FelipeMassa_p5fgzy.jpg', 1),
       (8, 'fernando.alonso', 'Password7@', 'fernando.alonso@gmail.com', '0883320768', 'UNBLOCKED', 'USER', 'ACTIVATED',
        'https://res.cloudinary.com/dm9jsxeij/image/upload/v1633585998/5de04c6cc99bc5f4a6257049bc1d04fc_ivbebd.jpg', 1),
       (9, 'kimi.raikkonen', 'Password7@', 'kimi.raikkonen@gmail.com', '0883320769', 'UNBLOCKED', 'USER', 'ACTIVATED',
        'https://res.cloudinary.com/dm9jsxeij/image/upload/v1633586134/kimi-1_nc9ahn.jpg', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping data for table vwallet.wallets: ~9 rows (approximately)
/*!40000 ALTER TABLE `wallets` DISABLE KEYS */;
INSERT INTO `wallets` (`wallet_id`, `balance`, `user_id`)
VALUES (1, 0.00, 2),
       (2, 419701.00, 3),
       (3, 100089.00, 4),
       (4, 444048.00, 5),
       (5, 210000.00, 6),
       (6, 70667.00, 7),
       (7, 647132.00, 8),
       (8, 888363.00, 9),
       (9, 0.00, 1);
/*!40000 ALTER TABLE `wallets` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
